#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <unordered_map>
#include <vector>
#ifndef SERIAL_HPP
#define SERIAL_HPP
namespace Serializer {
/**
 * Factory class for holding key-value pairs of (string, function)
 * The individual factory functions must take in some argument type reference
 * and output a pointer to Base.
 */
template <class Base> class Factory {
  public:
    /* template for factory functions*/
    template <typename T>
    using defunc = std::function<std::unique_ptr<Base>(T &)>;

    /**
     * helper method for calling the default constructor
     * CreateClass is constructed
     * ArgType is the argument type
     **/
    template <typename CreateClass, typename ArgType = void>
    static std::unique_ptr<Base> deserializeDefault(ArgType & /*in*/) {
        return std::make_unique<CreateClass>();
    }
    template <typename CreateClass>
    static std::unique_ptr<Base> deserializeDefault() {
        return std::make_unique<CreateClass>();
    }
    /**
     * Get the registry. Needs to be done this way to avoid the "static
     * initialization order fiasco."
     * Deserialize ArgType
     */
    template <typename ArgType>
    static std::map<std::string, defunc<ArgType>> &get_registry() {
        static std::map<std::string, defunc<ArgType>> registry;
        return registry;
    }
};

/**
 * Templated class for automatically (statically) registering factories.
 * Designed to be used in the CRTC, ie Derived derived from
 * FactoryConcept<Derived, Base>
 */
template <class Derived, class Base> struct FactoryConcept {
    virtual ~FactoryConcept() {
        if (!registered_) {
            std::cerr << "bad";
        }
    } // registerd_ needs to be accessed somewhere, otherwise it is opted away

    /**
     * Register a specific deserializer
     */
    template <class ArgType> static inline bool register_type() {
        auto &registry = Factory<Base>::template get_registry<ArgType>();
        registry[Derived::class_identifier] = Derived::deserialize_func;
        return true;
    }

    /* initialized in order to statically initialize the factories*/
    static const bool registered_;
};

/**
 * If you want to make the registration default to a different type, specialize
 * this and call register_type<NEW_TYPE_1>&&register_type<NEW_TYPE_2>
 */
template <class T, class Base>
const bool FactoryConcept<T, Base>::registered_ =
    FactoryConcept<T, Base>::register_type<std::istream>();

/**
 * Macros to make the boilerplate stuff easier
 * REGISTER_CLASS makes a factory with the same name as the class that calls
 * the default constructors.
 * REGISTER_FACTORY does the same, but affords registering specific functions
 * explicitly.
 */
#define REGISTER_CLASS(x, Base, args)                                          \
    struct x##Base##_Concept_ : FactoryConcept<x##Base##_Concept_, Base> {     \
        ~x##Base##_Concept_() {}                                               \
        static inline const std::string class_identifier = #x;                 \
        static inline const std::string docstring        = args;               \
        static inline auto deserialize_func =                                  \
            Factory<Base>::deserializeDefault<x>;                              \
    };

#define REGISTER_FACTORY(x, Base, foo, args)                                   \
    struct x##Base##_Concept_ : FactoryConcept<x##Base##_Concept_, Base> {     \
        ~x##Base##_Concept_() {}                                               \
        static inline const std::string class_identifier = #x;                 \
        static inline const std::string docstring        = args;               \
        static inline auto deserialize_func              = foo;                \
    };

}; // namespace Serializer
#endif
