#include "Serializer.hpp"
#include <cassert>
#include <iostream>
#include <sstream>
using namespace Serializer;
struct Shape {
    double area{1};
    virtual ~Shape() = default;
};
using ShapeFactory = Factory<Shape>;
struct Rectangle : public Shape {
    double aspect{1};
};
struct Circle : public Shape {
    double radius{1};
};
class RectangleParser : public FactoryConcept<RectangleParser, Shape> {
  public:
    // NOLINTNEXTLINE
    RectangleParser(){};
    // NOLINTNEXTLINE
    static inline const std::string class_identifier = "rectangle";
    // NOLINTNEXTLINE
    static inline auto deserialize_func =
        ShapeFactory::deserializeDefault<Rectangle, std::istream>;
};
;
REGISTER_FACTORY(circle, Shape,
                 (ShapeFactory::deserializeDefault<Circle, std::istream>),
                 "width\nlength");
int main() {
    assert(ShapeFactory::get_registry<std::istream>().count("rectangle"));
    assert(ShapeFactory::get_registry<std::istream>().count("circle"));
    for (auto &iter : ShapeFactory::get_registry<std::istream>()) {
        auto ss = std::stringstream("");
        std::cout << iter.first << "\n";
    }

    return 0;
}
